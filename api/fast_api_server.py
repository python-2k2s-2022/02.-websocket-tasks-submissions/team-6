import asyncio
from typing import List
from datetime import datetime

from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse
from starlette.responses import FileResponse

from websockets.exceptions import ConnectionClosedOK

from utils import get_easy_dicts_from_tinkoff
import db


app = FastAPI()

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <h1>TinkoffMap</h1>
        <ul id='tinkoff_easy_json'>
        </ul>
        <script>
            var client_id = Date.now()
            var ws = new WebSocket(`ws://localhost:8000/ws/${client_id}`);
            ws.onmessage = function(event) {
                var messages = document.getElementById('tinkoff_easy_json')
                var message = document.createElement('li')
                var content = event.data
                messages.innerText=content
            };
            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""


class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)

    async def send_personal_json(self, personal_dict: str, websocket: WebSocket):
        await websocket.send_json(personal_dict)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)


manager = ConnectionManager()


@app.get("/get_atm_info/")
async def get(city="Kazan", currency="USD"):
    return HTMLResponse(html)


@app.get("/")
def read_root():
    return FileResponse('templates/base.html')


@app.websocket("/ws/{client_id}")
async def websocket_endpoint(client_id, websocket: WebSocket, city="Kazan", currency="USD"):
    await manager.connect(websocket)
    try:
        personal_info = {"city": "Kazan", "currencies": "RUB"}
        personal_info_key = str(personal_info.values())

        if personal_info_key not in db.users_personals_settings.values():
            db.users_personals_settings[client_id] = personal_info_key
            dict_for_client = await get_easy_dicts_from_tinkoff()
            db.previous_responses[personal_info_key] = {"response": dict_for_client,
                                                        "time": datetime.now().timestamp()}

        elif datetime.now().timestamp() - db.previous_responses[personal_info_key]["time"] < 300:
            dict_for_client = await get_easy_dicts_from_tinkoff()
            db.previous_responses[personal_info_key] = {"response": dict_for_client,
                                                        "time": datetime.now().timestamp()}

        else:
            dict_for_client = db.previous_responses[personal_info_key]['response']

        await manager.send_personal_json(personal_dict=dict_for_client, websocket=websocket)
        await asyncio.sleep(10)

        while True:
            if datetime.now().timestamp() - db.previous_responses[personal_info_key]["time"] < 300:
                dict_for_client = db.previous_responses[personal_info_key]["response"]
            else:
                dict_for_client = await get_easy_dicts_from_tinkoff()
                db.previous_responses[personal_info_key] = {"response": dict_for_client,
                                                            "time": datetime.now().timestamp()}

            await manager.send_personal_json(personal_dict=dict_for_client, websocket=websocket)
            await asyncio.sleep(300)

    except ConnectionClosedOK:
        db.users_personals_settings.pop(client_id)
        manager.disconnect(websocket)
