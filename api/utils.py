import asyncio
import json

import aiohttp


async def get_dict_from_tinkoff():
    headers = {"accept": "*/*",
               "accept-encoding": "gzip, deflate, br",
               "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
               "content-type": "application/json",
               "dnt": "1",
               "origin": "https://www.tinkoff.ru",
               "referer": "https://www.tinkoff.ru/",
               "sec-ch-ua": 'Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98',
               "sec-ch-ua-mobile": "?0",
               "sec-ch-ua-platform": "macOS",
               "sec-fetch-dest": "empty",
               "sec-fetch-mode": "cors",
               "sec-fetch-site": "same-site",
               "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.109 Safari/537.36"
               }

    body = {
        "bounds": {
            "bottomLeft": {
                "lat": 55.77186345797556,
                "lng": 46.97956010258835
            },
            "topRight": {
                "lat": 56.3770819147586,
                "lng": 47.625006879932116
            }
        },
        "filters": {
            "banks": ["tcs"],
            "showUnavailable": True,
            "currencies": ["RUB"]
        },
        "zoom": 12
    }

    url = "https://api.tinkoff.ru/geo/withdraw/clusters"

    async with aiohttp.ClientSession() as session:
        response = await session.post(url=url, headers=headers, json=body)
        dict_with_response = await response.json()

        return dict_with_response


async def get_easy_dicts_from_tinkoff():
    easy_dicts = []
    # with open("api/json_with_response.txt", "r") as file:
    #     dict_with_response = json.loads(file.read())

    dict_with_response = await get_dict_from_tinkoff()

    for cluster in dict_with_response['payload']['clusters']:
        for point in cluster['points']:
            atm_dict = {"name": '',
                        "address": '',
                        "limits": []}

            atm_dict['name'] = point['brand']['name']
            atm_dict['address'] = point['address']
            atm_dict['limits'] = point['limits']

            easy_dicts.append(atm_dict)

    return easy_dicts

# asyncio.run(dict_with_response = asyncio.run(get_easy_dicts_from_tinkoff()))
# with open("json_with_response.txt", "w+") as file:
#     file.write(json.dumps(dict_with_response))
