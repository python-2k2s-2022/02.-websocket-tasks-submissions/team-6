import logging
import os

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from dotenv import load_dotenv

from aiogram import Bot, Dispatcher, types

load_dotenv()

BOT_TOKEN = os.getenv("BOT_TOKEN")

bot = Bot(token=BOT_TOKEN, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot, storage=MemoryStorage())
storage = MemoryStorage()

logging.basicConfig(format=u'%(filename)s [LINE:%(lineno)d] #%(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.INFO,
                    )