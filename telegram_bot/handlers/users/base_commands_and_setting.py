import asyncio

import websockets
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command

from telegram_bot.loader import dp
from telegram_bot.states.setting import Setting
from telegram_bot.handlers.text_for_handlers import dict_with_answers
from telegram_bot.utils import visual, my_websockets


@dp.message_handler(Command("start"), state=None)
async def start(message: types.Message):
    await message.answer(dict_with_answers["start"])

    #  ask city
    await Setting.first()


@dp.message_handler(state=Setting.city)
async def get_city(message: types.Message, state):
    await message.answer(dict_with_answers["get_city"])

    # ask currency
    await state.update_data(city=message.text)
    await Setting.next()


@dp.message_handler(state=Setting.currency)
async def get_currency(message: types.Message, state):
    await message.answer(dict_with_answers["get_currency"])
    await state.update_data(currency=message.text)

    data = await state.get_data()
    city, currency = data.get('city'), data.get('currency')

    # run loop for get info about atm's

    client_id = message.from_user.id
    my_websocket_class = my_websockets.MyWebSocketClientForTelegram()
    websocket = await my_websocket_class.add_client(client_id=client_id, city=city, currency=currency)

    while True:
        data = await state.get_data()
        stop = data.get('stop')

        if stop:
            await state.finish()
            await websocket.remove_client(client_id)
            break

        json_from_api = await websocket.recv()
        message_for_user = visual.get_message_from_json(json_from_api)

        await message.answer(message_for_user)

        await asyncio.sleep(300)



