import websockets
from api import db

def singleton(class_):
    instances = {}
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance

@singleton
class MyWebSocketClientForTelegram:
    def __init__(self):
        self._dict_with_connections = {}
        self._dict_with_websockets = {}

    async def add_client(self, client_id, city, currency):
        connection = websockets.connect(f"ws://127.0.0.1:8000/ws/{client_id}?city={city}&currency={currency}")
        websocket = await connection.__aenter__()

        self._dict_with_connections[client_id] = connection
        self._dict_with_websockets[client_id] = websocket

        return websocket

    async def remove_client(self, client_id):
        connection = self.dict_with_connections[client_id]
        connection.__aexit__()

        self._dict_with_connections.pop(client_id)
        self._dict_with_websockets.pop(client_id)
