import json


def get_message_from_json(json_from_api):
    easy_dicts = json.loads(json_from_api)
    list_with_atm_strings = []
    for atm in easy_dicts:
        list_with_money_info = atm['limits']
        list_with_money_string = []
        for money_info in list_with_money_info:
            money_string = f"{money_info['amount']}, {money_info['currency']}"
            list_with_money_string.append(money_string)

        address = atm["address"]
        final_money_string = '\n'.join(list_with_money_string)

        atm_string = f"{address}\n{final_money_string}"
        list_with_atm_strings.append(atm_string)

    message = '\n\n'.join(list_with_atm_strings)
    return message
