from aiogram.dispatcher.filters.state import StatesGroup, State


class Setting(StatesGroup):
    city = State()
    currency = State()
    stop = State()
